function Game(props) {
  const venue = props.venue;
  return (
    <div>
      <div id="venueHeader">Welcome to {venue}</div>
      <div id="container">
        <div id="teamOne">
          <Teams team="Home" teamName="Batman" teamImage="batmanlogo.jpg" />
        </div>
        <h1 id="versus">VS</h1>
        <div id="teamTwo">
          <Teams team="Visiting" teamName="Superman" teamImage="supermanlogo.png" />
        </div>
      </div>
    </div>
  );
}

