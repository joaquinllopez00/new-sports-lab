class Teams extends React.Component {
  state = {
    shotcount: 0,
    shotsmade: 0,
    score: 0,
    shotPercentage: 0,
    takenShot: false,
  };

  swishBasket = new Audio("Swish.mp3");
  missedBasket = new Audio("miss.mp3");

  randomNumber = () => {
    const num = Math.floor(Math.random() * 3);
    return num;
  };

  calculatePercentage = () => {
    const shotcount = Number(this.state.shotcount);
    const shotsmade = Number(this.state.shotsmade);

    this.setState((state, props) => ({
      shotPercentage: (shotsmade / shotcount) * 100,
    }));
  };

  shoot = (event) => {
    const shotcount = this.state.shotcount;
    const shotsmade = this.state.shotsmade;
    const shotChance = this.randomNumber();
    const score = this.state.score;

    {
      if (shotChance === 1) {
        this.setState((state, props) => ({
          score: state.score + 2,
          shotcount: state.shotcount + 1,
          shotsmade: shotChance === 2 || shotChance === 1 || shotChance === 3 ? state.shotsmade + 1 : state.shotsmade,
          takenShot: true,
        }));
        this.swishBasket.play();
      } else {
        this.setState((state, props) => ({
          shotcount: state.shotcount + 1,
          takenShot: true,
        }));
      }
      this.calculatePercentage();
    }

    {
      shotChance !== 1 ? this.missedBasket.play() : null;
    }
  };

  render() {
    const team = this.props.team;
    const teamName = this.props.teamName;
    return (
      <div>
        <h1 id="team">
          {" "}
          {team}: {teamName}
        </h1>
        <img src={this.props.teamImage} alt={this.props.team} />
        <div id="mainInfo">
          <div>Shots Attempted:{this.state.shotcount}</div>
          <div>Shots Made: {this.state.shotsmade}</div>
          <div>Score: {this.state.score} points</div>
          <button onClick={this.shoot}>Take a shot</button>
          <br />
          {this.state.takenShot === true && <div>Shot Percentage {this.state.shotPercentage}%</div>}
        </div>
      </div>
    );
  }
}
