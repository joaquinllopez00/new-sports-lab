// Deafault App component that all other compents are rendered through
function App(props) {
  return (
    <div id="game">
      <Game venue="Bankers Life Field" />
    </div>
  );
}

//Render the application
ReactDOM.render(<App />, document.getElementById("root"));
